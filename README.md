# CMS Test

This project is meant to test some of the most common content mangement systems:

* [Wordpress](https://wordpress.org/)
* [Joomla](https://www.joomla.org/)
* [Drupal](https://www.drupal.org/)
* [Django cms](https://www.django-cms.org/en/)

## Prerequisites

* [Docker Desktop](https://www.docker.com/products/docker-desktop)
* [Docker Compose](https://docs.docker.com/compose/)

## Installing
Use these instructions to install compartmentalised versions of each content management system.

### Wordpress
```bash
$ cd wordpress
$ docker-compose up -d
```
* Open browser and go to [http://localhost:8000](http://localhost:8000).
* Follow instructions to install clean instance of Wordpress.
* Open [http://localhost:8000/wp-admin/options-permalink.php](http://localhost:8000/wp-admin/options-permalink.php) and choose ```Post name```.
* Example of api for headless use: [http://localhost:8000/wp-json/wp/v2/posts](http://localhost:8000/wp-json/wp/v2/posts).

### Joomla
```bash
$ cd joomla
$ docker-compose up -d
```

* Open browser and go to [http://localhost:8001](http://localhost:8001).
* Fill out configuration form.
* Fill out database form. Use
	- host name: joomladb
	- user name: root
	- password: example
	- database name: joomladb
* Finish installation, fill website with test content at will


### Drupal
```bash
$ cd drupal
$ docker-compose up -d
```

* Open browser and go to [http://localhost:8080](http://localhost:8080).
* Fill out configuration form.
* During setup choose database type SQLite


### Django cms
```bash
$ cd django
$ docker-compose up -d
$ docker-compose exec django-cms sh
```
* After ```/app``` type ```./manage.py createsuperuser```
* Fill out configuration
* After ```/app``` type ```exit```.
* Open browser and go to [http://localhost:8002](http://localhost:8002).


### Clean up
To stop and remove all docker containers and images after testing use:
```bash
$ docker stop $(docker ps -aq)
$ docker rm $(docker ps -aq)
$ docker rmi $(docker images -q)
```

## Acknowledgments
* [vimagick](https://hub.docker.com/r/vimagick/django-cms/)
